import React from "react";
import { PromotionsDemo } from "./pages/PromotionsDemo/promotionsDemo";
import "./App.css";

function App() {
  return (
    <div>
      <PromotionsDemo />
    </div>
  );
}

export default App;
