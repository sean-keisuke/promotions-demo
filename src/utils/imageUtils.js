class Rectangle {
  constructor(x1, y1, x2, y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
  }
}

function isPointInside(lastpoint, rectangle) {
  // we technically don't have to check for y because we already check it in the parent function
  if (
    lastpoint.x > rectangle.x1 &&
    lastpoint.x < rectangle.x2 &&
    lastpoint.y > rectangle.y1 &&
    lastpoint.y < rectangle.y2
  ) {
    return true;
  }
  return false;
}

export function hitTest(lastPoint, actions) {
  var matchedimageObj = null;
  if (actions.length > 0) {
    actions.forEach((action) => {
      if (isPointInside(lastPoint, action.rectangleObj)) {
        matchedimageObj = action.imageObj;
      }
    });
  }
  return matchedimageObj;
}

export async function createActions(offsetTop, offsetLeft, actions) {
  // hydrate rectangle class ahead of time
  actions.forEach(
    (action, i) =>
      (actions[i].rectangleObj = new Rectangle(
        action.rectangle.x1 + offsetLeft,
        action.rectangle.y1 + offsetTop,
        action.rectangle.x2 + offsetLeft,
        action.rectangle.y2 + offsetTop
      ))
  );

  return await loadAllImages(actions);
}

export async function loadAllImages(actions) {
  var imagePromises = actions.map((a) => loadImage(a.imageURL));
  const loadedImageObjs = await Promise.all(imagePromises);
  // stuff the image objs back into the original actions and return a new actions struct
  const newActions = actions;
  newActions.forEach((action, i) => {
    action.imageObj = loadedImageObjs[i];
  });
  return newActions;
}

export async function loadImage(imageUrl) {
  return new Promise((resolve, reject) => {
    const image = new Image();
    image.src = imageUrl;
    image.onload = function () {
      resolve(image);
    };
    image.onerror = reject;
  });
}

export function setImage(canvas, ctx, imageObj) {
  if (imageObj !== null) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(imageObj, 0, 0);
  }
}