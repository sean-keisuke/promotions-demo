export async function getData(resource) {
  const res = await fetch(resource, {
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
  });
  return res.json();
}

