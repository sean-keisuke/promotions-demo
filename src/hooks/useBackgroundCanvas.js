import { useEffect, useRef } from "react";
import { setImage } from "../utils/imageUtils";

// Scaling Constants for Canvas
export const backgroundCanvasWidth = 691;
export const backgroundCanvasHeight = 185;

export function useBackgroundCanvas(imageObj) {
  const backgroundCanvasRef = useRef(null);

  useEffect(() => {
    const canvasObj = backgroundCanvasRef.current;
    const ctx = canvasObj.getContext("2d");
    // clear the canvas area before rendering the coordinates held in state
    ctx.clearRect(0, 0, backgroundCanvasWidth, backgroundCanvasHeight);
    setImage(canvasObj, ctx, imageObj);
  },[imageObj]);

  return [backgroundCanvasRef, backgroundCanvasWidth, backgroundCanvasHeight];
}
