import { useState, useEffect, useRef } from "react";

// Scaling Constants for Canvas

export const animationCanvasWidth = 691;
export const animationCanvasHeight = 185;
var circles = [];

//Random Circles creator
class create {
  constructor(coordinate, offsetTop, offsetLeft) {
    //Place the circles at the center
    this.x = coordinate.x - offsetLeft;
    this.y = coordinate.y - offsetTop;

    //Random radius in a range
    const radiusMin = 4;
    const radiusMax = 8;
    this.radius = radiusMin + Math.random() * (radiusMax - radiusMin);

    //Random velocities
    this.vx = -5 + Math.random() * 10;
    this.vy = -5 + Math.random() * 10;

    //Random colors
    const oranges = [
      "rgb(184, 142, 37, 0.5)",
      "rgb(235, 205, 131, 0.5)",
      "rgb(237, 172, 12, 0.5)",
      "rgb(179, 136, 30, 0.5)",
    ];
    this.rgb = oranges[Math.floor(Math.random() * oranges.length)];
    this.r = Math.round(Math.random()) * 255;
    this.g = Math.round(Math.random()) * 255;
    this.b = Math.round(Math.random()) * 255;
  }
}

export function useAnimationCanvas() {
  const animationCanvasRef = useRef(null);
  const [coordinate, setCoordinate] = useState(null);

  useEffect(() => {
    const canvasObj = animationCanvasRef.current;
    const { offsetTop, offsetLeft } = canvasObj.offsetParent;
    const ctx = canvasObj.getContext("2d");
    function draw(ctx) {
      // Clear canvas
      ctx.globalCompositeOperation = "source-over";
      ctx.fillStyle = "rgba(0,0,0,0)";
      ctx.clearRect(0, 0, animationCanvasWidth, animationCanvasHeight);
      ctx.fillRect(0, 0, animationCanvasWidth, animationCanvasHeight);

      //Fill the canvas with circles
      // iterate backwards so we can delete without probs
      for (var j = circles.length - 1; j >= 0; j--) {
        var c = circles[j];

        //Create the circles
        ctx.beginPath();
        ctx.arc(c.x, c.y, c.radius, 0, Math.PI * 2, false);
        ctx.fillStyle = c.rgb;
        ctx.fill();
        c.x += c.vx;
        c.y += c.vy;
        c.radius -= 0.12;

        if (c.radius < 0) {
          // delete the circle when it's off screen
          circles.splice(j, 1);
        }
      }
      if (circles.length > 0) {
        requestAnimationFrame(animate);
      }
    }
    function animate() {
      draw(ctx);
    }

    circles = [];
    if (coordinate) {
      for (var i = 0; i < 50; i++) {
        circles.push(new create(coordinate, offsetTop, offsetLeft));
      }
      requestAnimationFrame(animate);

      // clear the canvas area before rendering the coordinates held in state
      ctx.clearRect(0, 0, animationCanvasWidth, animationCanvasHeight);
    }
  }, [coordinate]);

  return [
    setCoordinate,
    animationCanvasRef,
    animationCanvasWidth,
    animationCanvasHeight,
  ];
}
