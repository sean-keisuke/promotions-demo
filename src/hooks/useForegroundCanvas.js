import { useState, useEffect, useRef } from "react";
import { setImage } from "../utils/imageUtils";

// Scaling Constants for Canvas

export const foregroundCanvasWidth = 691;
export const foregroundCanvasHeight = 185;

export function useForegroundCanvas(imagePromise) {
  const foregroundCanvasRef = useRef(null);
  const [coordinate, setCoordinate] = useState(null);
  const [imageObj, setImageObj] = useState(null);

  useEffect(() => {
    if (imagePromise !== null) {
      setImageObj(imagePromise);
    }

    const canvasObj = foregroundCanvasRef.current;
    if (canvasObj && canvasObj.offsetParent) {
      const ctx = canvasObj.getContext("2d");
      // clear the canvas area before rendering the coordinates held in state
      ctx.clearRect(0, 0, foregroundCanvasWidth, foregroundCanvasHeight);
      setImage(canvasObj, ctx, imageObj);
      if (coordinate !== null) {
        // hit
        setImageObj(imagePromise);
        setImage(canvasObj, ctx, imageObj);
      }
    }
  }, [imageObj, coordinate, imagePromise]);

  return [
    setCoordinate,
    foregroundCanvasRef,
    foregroundCanvasWidth,
    foregroundCanvasHeight,
  ];
}
