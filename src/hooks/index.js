import { useForegroundCanvas } from "./useForegroundCanvas";
import { useBackgroundCanvas } from "./useBackgroundCanvas";
import { useAnimationCanvas } from "./useAnimationCanvas";

export { useForegroundCanvas, useBackgroundCanvas, useAnimationCanvas };
