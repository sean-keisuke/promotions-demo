import React, { useEffect } from "react";
import { useForegroundCanvas } from "../../hooks/index";

function ForegroundCanvas({ currentCoord, imageObj }) {
  const [
    setForegroundCoordinate,
    foregroundCanvasRef,
    foregroundCanvasWidth,
    foregroundCanvasHeight,
    foundCoordinate,
  ] = useForegroundCanvas(imageObj);

  useEffect(() => {
    setForegroundCoordinate(currentCoord);
  }, [currentCoord, foundCoordinate, setForegroundCoordinate]);

  return (
    <canvas
      className="canvas"
      id="foreground-canvas"
      ref={foregroundCanvasRef}
      width={foregroundCanvasWidth}
      height={foregroundCanvasHeight}
    />
  );
}
export default ForegroundCanvas;
