import React, { useState, useRef, useEffect } from "react";
import BackgroundCanvas from "../backgroundCanvas";
import ForegroundCanvas from "../foregroundCanvas";
import AnimationCanvas from "../animationCanvas";
import LoadingSpinner from "../loadingSpinner";
import { getData } from "../../utils/apiUtils";
import { loadImage, createActions, hitTest } from "../../utils/imageUtils";

function SmasherCanvas() {
  const [backgroundImageObj, setBackgroundImageObj] = useState(null);
  const [currentCoord, setCurrentCoord] = useState(null);
  const [foundHit, setFoundHit] = useState(null);
  const [actions, setActions] = useState([]);
  const [loading, setLoading] = useState(false);
  const smasherRef = useRef();

  // If we just use Coordinate, we cannot
  const [foundCoord, setFoundCoord] = useState(null);

  useEffect(() => {
    async function fetchImages() {
      try {
        setLoading(true);
        const smasherObj = smasherRef.current;
        const { offsetTop, offsetLeft } = smasherObj;

        // grab data first
        const data = await getData("data.json");
        const foregroundImageURL = data.foregroundImgURL;
        const backgroundImageURL = data.backgroundImgURL;

        // then load images/actions
        const backgroundImg = await loadImage(backgroundImageURL);
        setBackgroundImageObj(backgroundImg);
        const foregroundImg = await loadImage(foregroundImageURL);
        setFoundHit(foregroundImg);
        const actions = await createActions(
          offsetTop,
          offsetLeft,
          data.actions
        );
        setActions(actions);
        setLoading(false);
      } catch (error) {
        setLoading(false);
        console.log(error);
      }
    }
    fetchImages();
  }, []);

  useEffect(() => {
    if (currentCoord !== null) {
      const hitObj = hitTest(currentCoord, actions);
      if (hitObj === null) {
        setFoundCoord(null);
      } else {
        setFoundHit(hitObj);
        setFoundCoord(currentCoord);
      }
    }
  }, [actions, currentCoord, foundHit]);

  const handleCoordinatesClick = (event) => {
    // on each click get current mouse location
    setCurrentCoord({ x: event.clientX, y: event.clientY });
  };

  return (
    <div>
      <ol>
        <li>Click on one of the 5 circles</li>
        <li>
          Based on which circle clicked, replace foreground with the appropriate
          image.
        </li>
        <li>Profit!</li>
      </ol>
      <div
        className="smasher-container"
        id="js-container"
        onClick={handleCoordinatesClick}
        ref={smasherRef}
      >
        {loading && <LoadingSpinner />}
        {!loading && (
          <div>
            <BackgroundCanvas imageObj={backgroundImageObj} />
            <ForegroundCanvas currentCoord={currentCoord} imageObj={foundHit} />
            <AnimationCanvas foundCoordinate={foundCoord} />
          </div>
        )}
      </div>
    </div>
  );
}
export default SmasherCanvas;
