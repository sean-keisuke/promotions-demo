import { useBackgroundCanvas } from "../../hooks/index";

function BackgroundCanvas({imageObj}) {
    const [
      backgroundCanvasRef,
      backgroundCanvasWidth,
      backgroundCanvasHeight,
    ] = useBackgroundCanvas(imageObj);

    return (
        <canvas
        className="canvas"
        id="background-canvas"
        ref={backgroundCanvasRef}
        width={backgroundCanvasWidth}
        height={backgroundCanvasHeight}
      />
    )
}

export default BackgroundCanvas