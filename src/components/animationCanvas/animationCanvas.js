import React, { useEffect } from "react";
import { useAnimationCanvas } from "../../hooks/index";

function AnimationCanvas({ foundCoordinate }) {
  const [
    setAnimationCoordinate,
    animationCanvasRef,
    animationCanvasWidth,
    animationCanvasHeight,
  ] = useAnimationCanvas();

  useEffect(() => {
    setAnimationCoordinate(foundCoordinate);
  }, [foundCoordinate, setAnimationCoordinate]);

  return (
    <canvas
      className="canvas"
      id="animation-canvas"
      ref={animationCanvasRef}
      width={animationCanvasWidth}
      height={animationCanvasHeight}
    />
  );
}

export default AnimationCanvas;
